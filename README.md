package to launch jackal with velodyne

## Requirements

This package depends on mouncef_joy and requires you to have already a catkin workspace (catkin_ws) 

## Quick start

First change to the source space directory of the catkin workspace
``` bash
$ cd ~/catkin_ws/src
```

clone the repository 

``` bash
$ git clone https://gitlab.com/jackal_enac/mouncef_velodyne
```

Now you need to build the package in the catkin workspace: 
``` bash
$ cd ~/catkin_ws
$ catkin_make
```

Now you need to run the following command to launch gazebo and rviz
``` bash
$ roslaunch mouncef_velodyne run.launch
```

This package also offers the option to chose between multiple worlds 

In order to change the world, run the following command
``` bash
$ roslaunch mouncef_velodyne run.launch world:=office.world
```

The supported worlds are the following : 
- abs.world          
- boxes.world        
- random_no_walls.world
- cylinders.world    
- scenario1.world
- jackal_race.world  
- terrain.world
- parking_lot.world  
- office.world 
- office_2.world       
- willow_garage.world
- random_1.world     
- random_2.world     
- random_3.world     
- random_4.world     
- random_5.world
- random_6.world


